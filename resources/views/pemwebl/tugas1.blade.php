<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas1-Biodata</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            background-color: #f0f0f0;
            padding: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
    </style>
</head>
<body>
    <h1>Biodata</h1>
    <table>
        <tr>
            <th>NIM</th>
            <td>:</td>
            <td>215150407111038</td>
        </tr>
        <tr>
            <th>Nama</th>
            <td>:</td>
            <td>Rafly Ramdhan Al Fauzi</td>
        </tr>
        <tr>
            <th>Alamat</th>
            <td>:</td>
            <td>Jl.Titan Asri 3</td>
        </tr>
        <tr>
            <th>Hobi</th>
            <td>:</td>
            <td>Memancing</td>
        </tr>
    </table>
</body>
</html>
