<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;

Route::get('/', [MahasiswaController::class, 'index']);
Route::post('/store', [MahasiswaController::class, 'store']);

Route::get('/tugas1', function () {
    return view('pemwebl.tugas1');
});